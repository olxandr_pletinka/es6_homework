interface CreateElementOptions {
  tagName:string;
  className?:string;
  attributes?: {
    [key: string]: string;  
  }
}

interface CreateModalOptions {
  title: string;
  bodyElement: any ;
  onClose: any;
}