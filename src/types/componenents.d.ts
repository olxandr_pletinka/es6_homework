
interface FighterProp {
  attack: number;
  defense: number;
  health: number;
  name: string;
  source: string;
  _id: string;
  block?: boolean;
  super?: boolean;
  fightHP?: number;
  position?: string;
}

interface FighterOptions {
  _id: string;
  name: string;
  source: string;
}

interface SelectedFighters {
  [index: number]:FighterProp;
  [index?: number]:FighterProp;
}



interface SelectedFighters extends Array<SelectedFighters> {};
interface FighterProp extends Array<FighterProp> {};
