import { createFighterImage } from '../fighterPreview';
import {showModal} from './modal'

export function showWinnerModal(fighter:any) {
  showModal({title: fighter.name, bodyElement: createFighterImage(fighter)});
}
