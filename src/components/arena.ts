import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import {fight} from './fight'
import {showWinnerModal} from './modal/winner'
import { fighters } from '../helpers/mockData';

export function renderArena(selectedFighters:SelectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);
  const [firstFighter, secondFighter] = selectedFighters
  if (root) {
    root.innerHTML = '';
    root.append(arena);
  }


  for (let i = 0; i < selectedFighters.length; i++) {
    selectedFighters[i].block = false;
    selectedFighters[i].super = true;
    selectedFighters[i].fightHP = selectedFighters[i].health;
    if (i == 0) {
      selectedFighters[i].position = 'left';
    } else {
      selectedFighters[i].position = 'right'
    }
  }
  
  fight(firstFighter, secondFighter).then(result => showWinnerModal(result));
}

function createArena(selectedFighters: SelectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const [firstFighter, secondFighter] = selectedFighters
  const healthIndicators = createHealthIndicators(firstFighter, secondFighter);
  const fighters = createFighters(firstFighter, secondFighter);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter:FighterProp, rightFighter:FighterProp) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter:FighterProp, position:string) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter:FighterProp, secondFighter:FighterProp) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter:FighterProp, position:string) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
