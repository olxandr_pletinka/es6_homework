import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter:FighterProp, position:string) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    let fighterName = createElement({
      tagName: 'span',
      className: 'arena___fighter-name',
    });

    fighterName.textContent = fighter.name;
    fighterElement.append(fighterName)
    fighterElement.append(createFighterImage(fighter));
    
    for (let prop in fighter) {
      if (prop == 'attack' || prop == 'defense' || prop == 'health') {
        let newElem = createElement({
          tagName: 'div',
          className: 'arena___fighter-name',
        });
        newElem.textContent  = `${prop}  ${fighter[prop]}\n`;
        fighterElement.append(newElem)
      }
    }
  } 
  
  return fighterElement;
}

export function createFighterImage(fighter:FighterProp) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
