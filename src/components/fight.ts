import { controls } from '../constants/controls';

export async function fight(firstFighter:FighterProp, secondFighter:FighterProp) {
  return new Promise((resolve) => {
    let superAttack = new Set();

    window.addEventListener('keydown', keyDown)
    window.addEventListener('keyup', keyUp)

    function brawl(attacker:FighterProp, defender:FighterProp, damage:number) {
      let healthFighter = document.getElementById(`${defender.position}-fighter-indicator`);
      
      if (defender.fightHP) {
        defender.fightHP -= damage;
        
        if (healthFighter) {
          healthFighter.style.width = `${(defender.fightHP / defender.health) * 100}%`;
        }
  
        if (defender.fightHP < 0) {
          resolve (attacker)
        }
      }
    }

    function criticalHit(fighter:FighterProp) {

      return 2 * fighter.attack;
    }

    function keyDown(e:any) {
      if (!e.repeat) {
        switch (e.code) {
          case controls.PlayerOneAttack:
              brawl(firstFighter, secondFighter, getDamage(firstFighter, secondFighter))
            break;

          case controls.PlayerTwoAttack:
              brawl(secondFighter, firstFighter, getDamage(secondFighter, firstFighter  ));
            break;
    
          case controls.PlayerOneBlock:
            firstFighter.block = true;
            break;

          case controls.PlayerTwoBlock:
            secondFighter.block = true;
            break;  
        }
      }
      superAttack.add(e.code);

      if (firstFighter.super) {
        if (superAttack.has(controls.PlayerOneCriticalHitCombination[0]) 
        && superAttack.has(controls.PlayerOneCriticalHitCombination[1]) 
        && superAttack.has(controls.PlayerOneCriticalHitCombination[2])) {
          
          brawl(firstFighter, secondFighter, criticalHit(firstFighter))
          firstFighter.super = false;
          setTimeout(() => {
            firstFighter.super = true;
          }, 10000);
        }
      }

      if (secondFighter.super) {
        if (superAttack.has(controls.PlayerTwoCriticalHitCombination[0]) 
        && superAttack.has(controls.PlayerTwoCriticalHitCombination[1]) 
        && superAttack.has(controls.PlayerTwoCriticalHitCombination[2])) {

          brawl(secondFighter, firstFighter, criticalHit(secondFighter));
          secondFighter.super = false;
          setTimeout(() => {
            secondFighter.super = true;
          }, 10000);
        }
      }
    } 
    
    function keyUp(e:any) {
      switch (e.code) {    
        case controls.PlayerOneBlock:
          firstFighter.block = false;
          break;

        case controls.PlayerTwoBlock:
          secondFighter.block = false;
          break;  
      }
      superAttack.delete(e.code);
    }
  });
}

export function getDamage(attacker:FighterProp, defender:FighterProp):number {
  let hitPower = (attacker.block || defender.block) ? 0 : getHitPower(attacker);
  let damage = hitPower - getBlockPower(defender);
 
  damage = (damage < 0) ? 0 : damage;

  return damage;
}

export function getHitPower(fighter:FighterProp):number {
  const criticalHitChance = 1 + Math.random(); 

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter:FighterProp):number {
  const dodgeChance = 1 + Math.random();

  return fighter.defense * dodgeChance;
}
