import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'visible';
      }

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      if (App.rootElement) {
        App.rootElement.appendChild(fightersElement);
      }
    } catch (error) {
      console.warn(error);
      if (App.rootElement) {
        App.rootElement.innerText = 'Failed to load data';
      }
      
    } finally {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
}

export default App;
